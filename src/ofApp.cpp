#include "ofApp.h"
#include "ofxCv.h"
//--------------------------------------------------------------
void ofApp::setup(){
    cam.setup(1000,1000);
    frame.allocate(1000, 1000, OF_IMAGE_COLOR);
    velocity = 0;
    gui.setup();
    
    gui.add(size.setup("size", 25, 10, 50));
    gui.add(BlacknWhite.setup("Black and White", true));
    gui.add(ContourOnly.setup("Contour Only", true));
    contourFinder.setMinAreaRadius(10);
    contourFinder.setMaxAreaRadius(150);
}

//--------------------------------------------------------------
void ofApp::update(){
    if(cam.isInitialized()){
        cam.update();
        velocity ++;
        if(cam.isFrameNew()){
            frame.setFromPixels(cam.getPixels());
            
        }
    }
    contourFinder.setTargetColor(targetColor, FracCos ? ofxCv::TRACK_COLOR_HS : ofxCv::TRACK_COLOR_RGB);
}

//--------------------------------------------------------------
void ofApp::draw(){
    
    float time = ofGetElapsedTimef();
    for(int x = 0; x < 1000; x+= size){
        for(int y = 0; y < 1000; y+= size){
            ofColor color = frame.getColor(x,y);
            color.setSaturation(100);
            color.setHue(color.getHue() - 50);
            
            ofSetColor(color);
            
            float brightness = color.getBrightness();
            
            
            if(ofGetMousePressed()){
                color.setHue(0);
                ofDrawRectangle(ofGetMouseX(), ofGetMouseY(), brightness/2, brightness/2);
            }
            contourFinder.draw();
            if(ContourOnly){
                
                ofSetColor(200);
                ofSetColor(100);
                
            }
               // cv::RotatedRect ellipse = contourFinder.getFitEllipse(i);
            
                //ofTranslate(30, 50);
               // ofRotate(velocity);
                //ofDrawEllipse(0, 0, 50, 50);
               
            if(BlacknWhite){
                ofFill();
                ofSetColor(brightness);
            }
            ofNoFill();
            ofPushMatrix();
            ofDrawSphere(x, y , brightness/5, brightness/5);
            ofPopMatrix();
        }
    }
    
    gui.draw();
}

