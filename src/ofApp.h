#pragma once

#include "ofMain.h"
#include "ofxGui.h"
#include "ofxCv.h"
class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

    ofVideoGrabber cam;
    ofImage frame;
    ofxCv::ContourFinder contourFinder;
    ofColor targetColor;
    
    ofxPanel gui;
    ofxIntSlider size;
    ofxToggle BlacknWhite;
    ofxToggle ContourOnly;
    float velocity;
};
