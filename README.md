# README #

![ ](/Screenshot.png)
![ ](/Screenshot1.png)

### What is this repository for? ###

This is an application of testing Computer Vision and play around with the image taken from the built-in webcam for the Advanced Coding class of University of Denver.
If you turn off the "Black and White" only,  you will get just the shade of your contour. Otherwise, you will get the whole picture made of from hundreds of circles, you can choose whether the picture is color or just black and white.
The code used the addon ofxCv, ofxGui, ofxOpenCv


### How do I get set up? ###

First, you have to import the whole folder into the ProjectGenerator. Then run the IDE on your computer. Finally, hit run on the IDE and enjoy



### Who do I talk to? ###
Created by Tam Nguyen
Contact via tam.t.nguyen@du.edu
